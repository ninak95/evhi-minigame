﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonController : MonoBehaviour {
    private Animator anim;
    public GameObject door;
    public bool reversed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject go = collision.gameObject;
        if (go.tag == "Player")
        {
            anim = GetComponent<Animator>();
            anim.SetBool("Pressed", true);
            //Debug.Log("colliding skull");
            //Debug.Log(anim.GetBool("Pressed"));
            door.GetComponent<doorController>().open = !reversed;
        }
           


    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        anim = GetComponent<Animator>();
        anim.SetBool("Pressed", false);
        //Debug.Log(" exit colliding skull");
        //Debug.Log(anim.GetBool("Pressed"));
    }
}
