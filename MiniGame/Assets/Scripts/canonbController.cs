﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canonbController : MonoBehaviour
{
    public bool facingRight = true;
    private Rigidbody2D rb2d;
    private float maxSpeed = 5f;
    private float moveForce = 365f;


    // Use this for initialization
    void Start()
    {
        Debug.Log(facingRight);
        rb2d = GetComponent<Rigidbody2D>();
        if (!facingRight)
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }

    // Update is called once per frame
    void Update()
    {
        int h = 1;
        if (!facingRight)
            h *= -1;
        //Debug.Log(facingRight);
        transform.Translate(Vector2.right * Time.deltaTime * 5f * h);

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject go = collision.gameObject;
        if (go.tag != "Player")
        {
            Destroy(gameObject);
        }

    }

}
