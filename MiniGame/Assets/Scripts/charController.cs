﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class charController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;
    private int life = 2;
    private bool invincible = false;

    private bool grounded = false;
    private bool groundDie = false;
    private bool fired = false;
    private Animator anim;
    private Rigidbody2D rb2d;
    private GameObject health;
    public GameObject canonb;
    public GameObject cont;
    public int movement = 0;
    public bool dark;
    public int counter = 0;

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        health = transform.Find("Health Bar").gameObject;

    }


    // Update is called once per frame
    void Update () {

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        groundDie = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Pickups"));


        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }
        anim.SetBool("dark", dark);

        if (invincible)
            counter += 1;

        if (counter > 20)
        {
            invincible = false;
            counter = 0;
        }
            

        if (Input.GetKeyDown(KeyCode.S) )
        {
            Vector2 pos = transform.position;
            Vector2 pos2 = new Vector2(pos.x + 1, pos.y + 0.3f);
            canonb.GetComponent<canonbController>().facingRight = true;

            if (!facingRight)
            {
                canonb.GetComponent<canonbController>().facingRight = false;
                pos2 = new Vector2(pos.x - 1, pos.y + 0.3f);
            }
               
                
            Instantiate(canonb, pos2, Quaternion.identity);
        }
        

    }


    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
           
        }
        if (life == 0 || groundDie)
        {
            Lose();
            cont.SetActive(true);
            Destroy(gameObject);
           // Debug.Log(life+"life");

        }
        if(health != null)
        {
            health.GetComponent<Animator>().SetInteger("val",life);
        }

    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject go = collision.gameObject;
        if (go.tag == "Star")
        {
            anim.SetTrigger("star");
            invincible = true;
            Destroy(go);
        }
        if (go.tag == "Heart")
        {
            anim.SetTrigger("life");
            if(life < 3)
                life += 1;
            Destroy(go);

        }
        if (go.tag == "Enemy")
        {
            if(!invincible)
                life -= 1;
            Debug.Log("enemy coll");
            Debug.Log(life);


        }
    }
    void Lose()
    {
        GameObject parentObject = Instantiate(transform.Find("Main Camera").gameObject) as GameObject;
        //cont.SetActive(true);
        

    }



}














