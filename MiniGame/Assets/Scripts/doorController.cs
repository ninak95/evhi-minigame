﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class doorController : MonoBehaviour {
    private Animator anim;
    private Collider2D col;
    public bool open;
    public int Lvl;
    public bool reversed;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        col = GetComponent<Collider2D>();
    }
	
	// Update is called once per frame
	void Update () {
        col.isTrigger = open;
        anim.SetBool("Open", open);
        if(!open && reversed)
        {
            SceneManager.LoadScene(Lvl);

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject go = collision.gameObject;
        if (open == true && go.tag == "Player" && !reversed)
            SceneManager.LoadScene(Lvl);


    }
    /*
    private void OnTriggerExit2D(Collider2D collision)
    {
        anim = GetComponent<Animator>();
        anim.SetBool("Open", false);
        Debug.Log(" exit colliding");
        Debug.Log(anim.GetBool("Open"));
    }
    */
}
