﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class enemyController : MonoBehaviour
{
    public int dir = 1;
    private int counter = 0;
    private Rigidbody2D rb2d;
    public int life = 2;
    private GameObject health;
    public bool follow;
    public GameObject win;
    private GameObject fe;
    private Animator anim;
   


    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        health = transform.Find("Health Bar").gameObject;
        fe = win.transform.Find("Felicitations").gameObject;

        

    }

    // Update is called once per frame
    void Update()
    {
        if (counter > 35 && !follow)
        {
            counter = 0;
            dir *= -1;
        }
        transform.Translate(Vector2.right * Time.deltaTime * 5f * dir);
        counter += 1;
        /*
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        */

        if (life == 0)
        {
            if (SceneManager.GetActiveScene().buildIndex == 4)
            {

                
                fe.SetActive(true);
                Debug.Log("win"+fe.GetComponentInChildren<Text>().text);

            }

            Destroy(gameObject);
            
              

        }
        if (health != null)
        {
            health.GetComponent<Animator>().SetInteger("val", life);
        }



    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject go = collision.gameObject;
        if (go.tag == "Bullet")
        {
            life -= 1;
            anim.SetTrigger("hit");

        }
    }
}
