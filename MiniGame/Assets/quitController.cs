﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class quitController : MonoBehaviour {
    public Button yourButton;
    // Use this for initialization
    void Start()
    {
        //Button btn = yourButton.GetComponent<Button>();
        yourButton.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        Application.Quit();
        Debug.Log("You have clicked the button!");
        

    }
}
